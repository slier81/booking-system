<?php

use utility\Form;
use utility\Image;
use utility\Cookie;
use utility\Request;
use utility\Database;
use utility\Validator;
use utility\Authentication;
use utility\validator\TextValidator;
use utility\validator\CompareValidator;
use utility\validator\SelectValidator;
use utility\validator\EmailValidator;
use utility\validator\NumberValidator;
use utility\validator\RegexValidator;
use utility\validator\FileValidator;
use utility\validator\CheckboxValidator;
use utility\authentication\AuthParamBuilder;

$form = new Form();
$request = new Request( $session );
$cookie = new Cookie();
$session->keepFlash( 'fb_data' );
$fb_data = $session->get( 'fb_data' );

if( $request->postToGet() ){

    $validator = new Validator();
    $validator->addValidator( 'username', new RegexValidator( 'username', $_POST['username'], '#[a-zA-Z0-9\._]#', [ 'filed' => 'username' ] ) );
    $validator->addValidator( 'password', new TextValidator( 'password', $_POST['password'], array( 'field' => 'password', 'length' => array( 4, 12 ), 'allow_num' => true ) ) );
    $validator->addValidator( 'r_password', new CompareValidator( 'r_password', $_POST['r_password'], $_POST['password'], 'Password', array( 'field' => 'repeat password' ) ) );
    $validator->addValidator( 'email', new EmailValidator( 'email', $_POST['email'], array( 'filed' => 'email' ) ) );
    $validator->addValidator( 'mobile_num', new NumberValidator( 'mobile_num', $_POST['mobile_num'], array( 'field' => 'mobile num', 'length' => array( 8, 14 ) ) ) );
    $validator->addValidator( 'full_name', new TextValidator( 'full_name', $_POST['full_name'], array( 'field' => 'full name', 'length' => array( 'max' => 50 ), 'allow_space' => true ) ) );
    $validator->addValidator( 'gender', new SelectValidator( 'gender', $_POST['gender'], array( 'field' => 'gender', 'required' => false ) ) );
    $validator->addValidator( 'address', new RegexValidator( 'address', $_POST['address'], '#^[a-zA-Z0-9,.\s\-\r\n]+$#', array( 'field' => 'address', 'required' => false ) ) );
    $validator->addValidator( 'facebook_url', new RegexValidator( 'facebook_url', $_POST['facebook_url'], '#https?\://(?:www\.)?facebook\.com/(?:\d+|[A-Za-z0-9\.]+)/?#', array( 'field' => 'facebook url', 'required' => false ) ) );
    $validator->addValidator( 'user_image', new FileValidator( 'user_image', $_FILES['user_image'], array( 'jpg', 'jpeg', 'png' ), array( 'field' => 'image', 'required' => false ) ) );
    $validator->addValidator( 'newsletter', new CheckboxValidator( 'newsletter', @$_POST['newsletter'], array( 'field' => 'newsletter', 'required' => false ) ) );

    if( $validator->isValid() ){

        $db = new Database( DBDSN, DBUSER, DBPASS );
        $is_user = $db->select( 'users' )->where( sprintf( "username='%s'", $_POST['username'] ) )->totalrow()->execute();

        if( !$is_user ){

            $gender = $_POST['gender'] ? : null;
            $address = $_POST['address'] ? : null;
            $facebook_url = $_POST['facebook_url'] ? : null;
            $newsletter = @$_POST['newsletter'] ? : null;
            $user_image = $_FILES['user_image']['name'] ? : null;
            $user_thumb = $_FILES['user_image']['name'] ? : null;

            if( $user_image ){
                umask( 0 );

                if( !is_dir( USR_IMG_PATH . $_POST['username'] ) ){
                    mkdir( USR_IMG_PATH . $_POST['username'] );
                }

                $user_image = $_POST['username'] . '.png';
                $user_thumb = $_POST['username'] . '_thumb.png';

                $image = new Image( $_FILES['user_image'], [ 'jpg', 'gif', 'png' ] );

                $image->resize( USR_IMG_WDTH, USR_IMG_HGHT, 'auto' );
                $image->save( USR_IMG_PATH . $_POST['username'] . DS . $user_image );

                $image->resize( USR_THMB_WDTH, USR_THMB_HGHT, 'auto' );
                $image->save( USR_IMG_PATH . $_POST['username'] . DS . $user_thumb );
            }

            $db->insert( 'users',
                [ 'username' => '?', 'password' => '?', 'level' => 'user' ],
                [ $_POST['username'], password_hash( $_POST['password'], PASSWORD_DEFAULT ) ] )
                ->execute();

            $user_id = $db->getLastInsertId();

            $db->insert( 'user_profiles',
                [ 'user_id' => '?', 'full_name' => '?', 'gender' => '?', 'email' => '?', 'mobile_num' => '?', 'address' => '?' ],
                [ $user_id, $_POST['full_name'], $gender, $_POST['email'], $_POST['mobile_num'], $address ] )
                ->execute();

            $db->insert( 'user_settings',
                [ 'user_id' => '?', 'facebook_url' => '?', 'user_image' => '?', 'user_thumb' => '?', 'newsletter' => '?' ],
                [ $user_id, $facebook_url, $user_image, $user_thumb, $newsletter ] )
                ->execute();

            $auth = new Authentication( $session, $cookie );
            $builder = new AuthParamBuilder();
            $auth_param = $builder->setDatabase( $db )->setQuery( 'SELECT * FROM users WHERE username=?', [ $fb_data['username'] ] )->build();
            $auth->openidLogin( $auth_param );
            $session->redirect( SITE_URL . 'user' );
        }

        $validator->invalidateValidation( 'Username taken' );
    }
}

$template->sets( get_defined_vars() );
$template->display( 'main/layout.php', [ 'content' => $template->fetch( 'main/fb_register.php' ) ] );