<?php
use facebook\Facebook;
use facebook\FacebookApiException;
use utility\Authentication;
use utility\Database;
use utility\authentication\AuthParamBuilder;
use utility\Cookie;

$cookie = new Cookie();
$facebook = new Facebook( array(
    'appId' => FB_APP_ID,
    'secret' => FB_APP_SECRET,
) );

$user = $facebook->getUser();

if( $user ){
    try{
        $user_profile = $facebook->api( '/me' );
    }
    catch( FacebookApiException $e ){
        error_log( $e );
    }
}


if( $user ){

    $db = new Database( DBDSN, DBUSER, DBPASS );
    $is_user = $db->select( 'users' )->where( sprintf( 'username="%s"', $user_profile['username'] ) )->totalrow()->execute();

    if( $is_user ){
        #he does, so authenticate it
        $auth = new Authentication( $session, $cookie );
        $builder = new AuthParamBuilder();
        $auth_param = $builder->setDatabase( $db )->setQuery( 'SELECT * FROM users WHERE username=?', [ $user_profile['username'] ] )->build();
        $auth->openidLogin( $auth_param );
        $session->redirect( SITE_URL . 'user' );
    }
    else{
        $session->flash( 'fb_data', $user_profile );
        $session->redirect( SITE_URL . 'fb_register.php' );
    }
}

else{
    printf( '<a href="%s">Status</a><br>', $facebook->getLoginStatusUrl() );
    printf( '<a href="%s">Login</a>', $facebook->getLoginUrl( array( 'scope' => 'email' ) ) );
}

?>
