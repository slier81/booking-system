<?php print $form->formStart( 'register.php', array( 'upload' => true ) ); ?>
    Username:<?php print $form->text( 'username' ); ?> *<br>
    Password:<?php print $form->password( 'password' ); ?> *<br>
    Repeat Password:<?php print $form->password( 'r_password' ); ?>* <br>
    Email:<?php print $form->text( 'email' ); ?> *<br>
    Mobile Num:<?php print $form->text( 'mobile_num' ); ?>* <br>
    Full Name:<?php print $form->text( 'full_name' ); ?>* <br>
    Gender:<?php print $form->select( 'gender', [ '' => 'Please select', 'male' => 'Male', 'female' => 'Female' ] ); ?> <br>
    Address:<?php print $form->textarea( 'address' ); ?> <br>
    Facebook:<?php print $form->text( 'facebook_url' ); ?> <br>
    Image:<?php print $form->file( 'user_image' ); ?> <br>
    Newsleter:<?php print $form->checkbox( 'newsletter', 1 ); ?> <br>
<?php print $form->submit( 'Submit' ); ?> <br>
<?php print $form->formEnd(); ?>