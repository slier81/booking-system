<?php

use utility\Form;
use utility\Request;
use utility\Authentication;
use utility\Cookie;
use utility\Database;
use utility\Validator;
use utility\validator\TextValidator;
use utility\validator\RegexValidator;
use utility\authentication\AuthParam;
use utility\authentication\AuthParamBuilder;

$form = new Form();
$request = new Request( $session );
$cookie = new Cookie();

if( $request->postToGet() ){

    $validator = new Validator();
    $validator->addValidator( 'username', new RegexValidator('username', $_POST['username'], '#[a-zA-Z0-9\._]+#') );
    $validator->addValidator( 'password', new TextValidator( 'password', $_POST['password'], [ 'allow_num' => true ] ) );

    if( $validator->isValid() ){

        $auth = new Authentication( $session, $cookie );
        $db = new Database( DBDSN, DBUSER, DBPASS );

        $builder = new AuthParamBuilder();
        $auth_param = $builder->setDatabase( $db )
            ->setQuery( 'SELECT * FROM users WHERE username=?', [ $_POST['username'] ] )
            ->setPassword( $_POST['password'] )
            ->setPasswordcolumn( 'password' )->setRemember( @$_POST['remember'] )->build();

        if( $auth->login( $auth_param ) ){
            $db->update( 'users', [ 'last_login' => '?' ], [ date( "Y-m-d H:i:s" ) ] )->where( sprintf( 'username="%s"', $_POST['username'] ) )->execute();
            $location_mapper = [ 'user' => SITE_URL . 'user', 'merchant' => SITE_URL . 'merchant', 'admin' => SITE_URL . 'admin' ];
            $auth_data = $auth->getAuthData();
            $session->redirect( $location_mapper[$auth_data['level']] );
        }

        $validator->invalidateValidation( 'Invalid username or password' );
    }
}

$template->sets( get_defined_vars() );
$template->display( 'main/layout.php', [ 'content' => $template->fetch( 'main/login.php' ) ] );