<?php

define( 'DS', DIRECTORY_SEPARATOR );
define( 'ROOT_PATH', dirname( dirname( __FILE__ ) ) . DS ); #absolute root path
define( 'REL_PATH', substr( dirname( dirname( __FILE__ ) ), strlen( $_SERVER['DOCUMENT_ROOT'] ) ) ); #relative root path
define( 'SITE_URL', REL_PATH . DS );
define( 'LIB_PATH', ROOT_PATH . 'lib' . DS );
define( 'MODEL_PATH', ROOT_PATH . 'model' . DS );
define( 'VIEW_PATH', ROOT_PATH . 'view' . DS );
define( 'CSS_PATH', SITE_URL . 'asset/css' . DS );
define( 'JS_PATH', SITE_URL . 'asset/js' . DS );
define( 'IMG_PATH', SITE_URL . 'asset/img' . DS );
define( 'USR_IMG_PATH', IMG_PATH . 'user' . DS );
define( 'DBUSER', 'root' );
define( 'DBPASS', 'root' );
define( 'DBDSN', 'mysql:host=localhost;dbname=booking' );
define( 'USR_IMG_WDTH', 600 );
define( 'USR_IMG_HGHT', 600 );
define( 'USR_THMB_WDTH', 200 );
define( 'USR_THMB_HGHT', 200 );
define( 'FB_APP_ID', '741107019236881' );
define( 'FB_APP_SECRET', '63c7decf44ea2f83d41262192ae1e60e' );
define( 'PERPAGE', 3 );