<?php

include( 'init.php' );

use utility\Cookie;
use utility\Authentication;

$cookie = new Cookie();
$auth = new Authentication( $session, $cookie );

if( !$auth->isAuth() ) {
    $session->flash( 'err_msg', 'Please login first' );
    $session->redirect( SITE_URL . 'login.php' );
}

$auth_data = $auth->getAuthData();

if( $auth_data['level'] != 'user' ) {
    $session->flash( 'err_msg', 'Insufficient privillege' );
    $session->redirect( SITE_URL . 'login.php' );
}

