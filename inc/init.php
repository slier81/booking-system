<?php

include 'vars.php';
include LIB_PATH . 'utility/autoload.php';

use utility\AutoLoad;
use utility\ExceptionHandler;
use utility\Session;
use utility\Template;

new AutoLoad( LIB_PATH );
new ExceptionHandler();

$site_title = 'Booking System';
$session = new Session();
$template = new Template( VIEW_PATH );