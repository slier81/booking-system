<?php

use utility\Cookie;
use utility\Authentication;

$cookie = new Cookie();
$auth = new Authentication( $session, $cookie );

if( $auth->logout() ) {
    $session->flash( 'suc_msg', 'You have logged out' );
    $session->redirect( SITE_URL . 'login.php' );
}